package com.company.project_name.qa;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static com.company.qa.assertions.Assertions.assertPollTitle;

class SearchPageActionsTest extends CompanyProjectTest {

  @ParameterizedTest
  @MethodSource("parametersForTestSearch")
  void testSearch(String searchQuery, String expectedSearchResult) {
    googleHomePageTestSteps.search(searchQuery).clickSearchResultLink(expectedSearchResult);
    assertPollTitle(ext, expectedSearchResult, timeout);
  }

  static Stream<Arguments> parametersForTestSearch() {
    return Stream.of(
      Arguments.of("java", "Download Free Java Software"),
      Arguments.of("junit5", "JUnit 5"),
      Arguments.of("selenium", "Selenium - Web Browser Automation")
    );
  }
}