package com.company.project_name.qa.pages;

import org.openqa.selenium.By;

public abstract class GooglePage {

	protected static final String TITLE = "Google";

	protected static final By SEARCH_FIELD = By.cssSelector("input[title='Search']");
}
