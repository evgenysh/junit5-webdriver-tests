package com.company.project_name.qa;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import com.company.project_name.qa.test_steps.GoogleHomePageTestSteps;
import com.company.qa.SeleniumTest;
import com.company.qa.utilities.JsonUtil;

public abstract class CompanyProjectTest extends SeleniumTest {

	protected static URL baseUrl;

	protected long timeout = getTimeout();

	protected GoogleHomePageTestSteps googleHomePageTestSteps;

	@BeforeAll
  static void beforeAll() throws MalformedURLException {
    baseUrl = new URL(JsonUtil.getJsonValue("baseUrl", "TestData.json"));
  }

  @BeforeEach
  void setUp() {
    ext.navigateTo(baseUrl);
	  googleHomePageTestSteps = new GoogleHomePageTestSteps(ext);
  }
}