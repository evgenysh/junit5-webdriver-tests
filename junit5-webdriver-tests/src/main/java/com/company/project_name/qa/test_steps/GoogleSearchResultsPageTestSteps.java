package com.company.project_name.qa.test_steps;

import com.company.project_name.qa.CompanyProjectTest;
import com.company.project_name.qa.pages.GoogleSearchResultsPage;
import com.company.qa.extensions.WebDriverExt;
import com.company.qa.extensions.WebElementExt;

import static com.company.qa.assertions.Assertions.assertPollClickable;
import static com.company.qa.assertions.Assertions.assertPollTitle;
import static com.company.qa.assertions.Assertions.assertPollUrlChange;
import static com.company.qa.assertions.Assertions.assertPollVisibleBy;

public class GoogleSearchResultsPageTestSteps extends CompanyProjectTest {

	private WebDriverExt ext;

	public GoogleSearchResultsPageTestSteps(WebDriverExt ext, String searchQuery) {
		this.ext = ext;
		assertPollTitle(ext, GoogleSearchResultsPage.getTitle(searchQuery), timeout);
	}

	public void clickSearchResultLink(String linkText) {
		assertPollUrlChange(ext, assertPollClickable(ext, findSearchResultLink(linkText), timeout)::click, timeout);
	}

	private WebElementExt findSearchResultLink(String linkText) {
		return assertPollVisibleBy(ext, GoogleSearchResultsPage.SEARCH_RESULTS_LIST, timeout)
			.pollElementBy(GoogleSearchResultsPage.getSearchResultLink(linkText), timeout);
	}
}
