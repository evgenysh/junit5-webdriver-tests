package com.company.project_name.qa.test_steps;

import com.company.project_name.qa.CompanyProjectTest;
import com.company.project_name.qa.pages.GoogleHomePage;
import com.company.qa.extensions.WebDriverExt;

import static com.company.qa.assertions.Assertions.assertPollTitle;
import static com.company.qa.assertions.Assertions.assertPollUrlChange;
import static com.company.qa.assertions.Assertions.assertPollVisibleBy;

public class GoogleHomePageTestSteps extends CompanyProjectTest {

	private WebDriverExt ext;

	public GoogleHomePageTestSteps(WebDriverExt ext) {
		this.ext = ext;
		assertPollTitle(ext, GoogleHomePage.TITLE, timeout);
	}

	public GoogleSearchResultsPageTestSteps search(final String searchQuery) {
		assertPollUrlChange(ext,
			() -> assertPollVisibleBy(ext, GoogleHomePage.SEARCH_FIELD, timeout).clearExt().sendKeysAndSubmit(searchQuery),
			timeout);
		return new GoogleSearchResultsPageTestSteps(ext, searchQuery);
	}
}
