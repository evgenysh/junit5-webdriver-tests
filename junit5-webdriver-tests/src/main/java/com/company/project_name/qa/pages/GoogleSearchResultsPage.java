package com.company.project_name.qa.pages;

import org.openqa.selenium.By;

public abstract class GoogleSearchResultsPage extends GooglePage {

	public static final By SEARCH_RESULTS_LIST = By.id("search");

	public static String getTitle(String searchQuery) {
		return searchQuery + " - Google Search";
	}

	public static By getSearchResultLink(String linkText) {
		return By.linkText(linkText);
	}
}
