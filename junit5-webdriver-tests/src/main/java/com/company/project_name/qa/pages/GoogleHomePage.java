package com.company.project_name.qa.pages;

import org.openqa.selenium.By;

public abstract class GoogleHomePage extends GooglePage {

	public static final String TITLE = GooglePage.TITLE;

	public static final By SEARCH_FIELD = GooglePage.SEARCH_FIELD;
}
