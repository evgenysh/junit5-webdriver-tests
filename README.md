[![CircleCI][circleci status badge]][circleci project builds]
### This is a Selenium test project based on the [`junit5+webdriver`][1] standalone project.
---
#### Required:
- Java 9
- Apache Maven 3.5.0
#### Installation:
- clone this project recursively
- if you don't have access to the submodule, download the [*jar*][2] and add it to the project's build path
- run `mvn install -DskipTests`
#### Running tests:
- choose a browser to run the tests: `-DDRIVER_PROVIDER={browser name}DriverProvider`
- available providers(browser names): `Chrome`, `Firefox`, `IE`
- a remote driver provider `BrowserStack` is also available, to use it, provide your user name and API key in the configuration file [`cloudTestServiceConfig.xml`][3]
- run the tests `mvn test -DDRIVER_PROVIDER=ChromeDriverProvider`
- run the tests in the headless mode: `mvn test -DDRIVER_PROVIDER=FirefoxDriverProvider -Dheadless=true`
#### Notes:
- currently, only Chrome and Firefox browsers can be run in the headless mode
- browser driver executables, if not found in the default directory, will be downloaded
- the default driver directory is the user home directory and can be changed with `-DdriverPath='your path'`
- if you wish to update drivers, use `-DupdateDriver=true`

[circleci status badge]:https://circleci.com/bb/evgenysh/junit5-webdriver-tests.svg?style=shield&circle-token=7d95211cfaff4059fbbd937544c095a610eea61a
[circleci project builds]:https://circleci.com/bb/evgenysh/junit5-webdriver-tests

[1]:https://bitbucket.org/evgenysh/junit5-webdriver
[2]:https://storage.googleapis.com/junit5-webdriver/junit5-webdriver.jar
[3]:https://bitbucket.org/evgenysh/junit5-webdriver/src/master/src/main/resources/cloudTestServiceConfig.xml